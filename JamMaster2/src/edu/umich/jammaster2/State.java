package edu.umich.jammaster2;

public enum State { 
	ON(true), 
	OFF(false), 
	SELECTED(true);
	
	boolean isSomeFormOfOn;
	
	private State(boolean isSomeFormOfOn) 
	{
		this.isSomeFormOfOn = isSomeFormOfOn;
	}
	
	/*
	 * - I made this so that it's easier to check for ON || SELECTED. That's 
	 * 		what this returns. You can use it like "state.isOn()". 
	 */
	public boolean isOn() { return isSomeFormOfOn; }
};
