package edu.umich.jammaster2;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import ask.scanninglibrary.ASKActivity;
import ask.scanninglibrary.views.ASKAlertDialog;

public class SaveFileActivity extends ASKActivity 
{
	private EditText text;
	private Song song;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_save_file);
		
		Intent i = getIntent();
		song = (Song) i.getParcelableExtra("song");
		
		SimpleDateFormat df = new SimpleDateFormat("MM-dd-yy", Locale.US);
        String dateAndTime = df.format(new Date());
		
        int count = 0;
		File dirFile = MainActivity.self().getFilesDir();
		for (String strFile : dirFile.list())
			if (strFile.startsWith(dateAndTime))
				count++;
		
		if (count > 0) dateAndTime += "(" + count + ")";
		text = (EditText) findViewById(R.id.editText1);
		text.setText(dateAndTime);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.save_file, menu);
		return true;
	}

	public void Save(View view)
	{
		String name = text.getText().toString();
		
		if (name.length() == 0)
		{
			ASKAlertDialog.Builder dlgAlert  = new ASKAlertDialog.Builder(this);                      
		    dlgAlert.setMessage("You must enter a name for your song.");
		    dlgAlert.setTitle("File Error");              
		    dlgAlert.setPositiveButton("OK", null);
		    dlgAlert.setCancelable(true);
		    dlgAlert.create().show();
		    return;
		}
		
		try 
		{ 
			setResult(Activity.RESULT_OK, new Intent());
			song.setName(name);
			song.save(); 
			finish();
		} 
		
		catch (Exception e) 
		{
			ASKAlertDialog.Builder dlgAlert  = new ASKAlertDialog.Builder(this);                      
		    dlgAlert.setMessage("Could not save file " + name);
		    dlgAlert.setTitle("File Error");              
		    dlgAlert.setPositiveButton("OK", null);
		    dlgAlert.setCancelable(true);
		    dlgAlert.create().show();
		}
	}
	
	public void Back(View view)
	{
		setResult(Activity.RESULT_CANCELED, null);
		finish();
	}
}
