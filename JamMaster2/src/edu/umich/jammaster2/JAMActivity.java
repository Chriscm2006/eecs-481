package edu.umich.jammaster2;

import android.os.Bundle;
import ask.scanninglibrary.ASKActivity;
import ask.scanninglibrary.SelectionMethod;

public class JAMActivity extends ASKActivity
{
	SelectionMethod mOurInterface;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		mOurInterface = new JAMScanningMethod(this);
	}
	
	@Override
    protected void onPause() {
    	super.onPause();
    	mOurInterface.stopAcceptingInput();
    }

	@Override
    protected void onStop() {
    	super.onStop();
    	mOurInterface.stopAcceptingInput();
    }
	
	@Override
	public void stopScanning() {
		mOurInterface.stopAcceptingInput();
	}
	
	@Override
	public void startScanning() {
		if (mDoScan) mOurInterface.acceptInput(createScannable());
	}
}
