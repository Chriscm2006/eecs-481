package edu.umich.jammaster2;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

/*
 * - This represents an adapter that stores the grid information. 
 * - It does not let you put it in a state where it has no columns or no rows. 
 */
public class GridAdapter extends ArrayAdapter<Boolean> 
{
	private static final int minRows = 1;
	private static final int minColumns = 1;
	private final int numRows;
	private Drawable on, off, selected;
	private ArrayList<ArrayList<State>> mData; // indexed by column, then row
	
	/*
	 * - creates one Column. 
	 */
	public GridAdapter(Context context, int numRows) 
	{
		this(context, numRows, 1);
	}
	
	public GridAdapter(Context context, int numRows, int numColumns) 
	{
		super(context, 0);
		
		assert(numRows >= minRows);
		assert(numColumns >= minColumns);

		this.numRows = numRows;
		mData = new ArrayList<ArrayList<State>>(); 
		
		setNumColumns(numColumns);
		
		Resources res = context.getResources();
		on = res.getDrawable( R.drawable.note_on );
		off = res.getDrawable( R.drawable.note_off );
		selected = res.getDrawable( R.drawable.note_selected);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout rowView = (RelativeLayout) inflater.inflate(R.layout.image_button_cell, parent, false);
		ImageButton button = (ImageButton) rowView.findViewById(R.id.imageButton0);
		
		if (getState(position) == State.ON)			button.setImageDrawable(on);
		if (getState(position) == State.OFF)		button.setImageDrawable(off);
		if (getState(position) == State.SELECTED) 	button.setImageDrawable(selected);

		button.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View arg0) 
			{
				State value = getState(position);
				
				if (value == State.ON || value == State.SELECTED)
					setValue(position, State.OFF);
				else
					setValue(position, State.ON);
				
				GridIndex coords = gridIndex(position);
				MixerPlayer.getInstance().updateSongCell(coords);
				
				notifyDataSetChanged();
			}
		});
		
		return rowView;
	}
	
	@Override
	public void add(Boolean value) 
	{
		assert(false);
		//Don't use this method adding a single value invalidates 2 dimensional symmetry
	}
	
	@Override
	public int getCount() 
	{
		return getNumRows() * getNumColumns();
	}
	
	public int getRowSize() { return mData.size(); }
	
	public int getColumnSize() { return numRows; }
	
	public int getNumRows() { return getColumnSize(); }
	
	public int getNumColumns() { return getRowSize(); }
	
	public State getState(int position) 
	{
		GridIndex index = gridIndex(position);
		return getState(index.row, index.column);
	}
	
	public State getState(int row, int column) 
	{
		return mData.get(column).get(row);
	}
	
	public void setValue(int position, State value) 
	{
		GridIndex index = gridIndex(position);
		setValue(index.row, index.column, value);
	}
	
	public void setValue(int row, int column, State value) 
	{
		mData.get(column).set(row, value);
		notifyDataSetChanged();
	}
	
	public void clearAll()
	{
		for (int r = 0; r < getColumnSize(); r++)
		{
			for (int c = 0; c < getRowSize(); c++)
			{
				mData.get(c).set(r, State.OFF);
			}
		}
		
		notifyDataSetChanged();
	}
	
	public void setNumColumns(int num) 
	{
		assert(num >= 1);
		
		while (getNumColumns() < num)
		{
			addColumn(State.OFF);
		}
		
		while (getNumColumns() > num)
		{
			removeColumn();
		}
	}
	
	public void removeColumn() 
	{
		mData.remove(mData.size() - 1);
	}
	
	public void addColumn(State value) 
	{
		ArrayList<State> temp = new ArrayList<State>();
		mData.add(temp);
		
		for (int i = 0; i < numRows; i++) 
		{
			temp.add(value);
		}
	}
	
	/*
	 * - 'position' indexes into the array you'd get if you laid out the columns end 
	 * 		to end (with low to high row indices). 
	 */
	private GridIndex gridIndex(int gridViewPosition) 
	{
		int columnIndexFromEnd = gridViewPosition / numRows;
		
		return new GridIndex(gridViewPosition % numRows, 
				getNumColumns() - columnIndexFromEnd - 1);
	}
}
