package edu.umich.jammaster2;

public interface ReadOnlySong {
	public String getName();
	public int getNumRows();
	public int getNumColumns();
	public State getCellState(int row, int column);
	public String getSoundArrayName();
	public int getSoundResID(int row);
	public String[] getSoundNameArray();
	public int getTempo();
	public int getNumActiveRows(int column);
}
