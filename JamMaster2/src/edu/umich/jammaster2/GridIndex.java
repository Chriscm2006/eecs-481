package edu.umich.jammaster2;

/*
 * - This class is immutable. Don't add setters. 
 */
public class GridIndex {
	public final int row;
	public final int column;
	
	GridIndex(int row, int column) {
		this.row = row;
		this.column = column;
	}
}
