package edu.umich.jammaster2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.widget.GridView;

/*
 * - This represents a collection of Sounds assigned to a grid, and is indexed the same way (by row). 
 * - Pretty much the only utility of this class is that of a struct. 
 * - A Song cannot have no columns. 
 */
public class Song implements ReadOnlySong, Parcelable {
	private static final String fileExtension = ".song";
	private static final int MIN_COLUMNS = 1;
	
	private GridAdapter mAdapter;
	private int tempoIndex = 1;
	private SoundResIDArray soundResIDArray;
	private String name = "song";
	private int tempoArray[] = new int[] {40, 60, 80, 96, 120, 140, 180, 200, 250, 350, 500};// in columns per min.
	
	/*
	 * - 'songFileName' should not include any suffix. 
	 * - 'songFileName' should be the name of a Song file that has been saved by 'save()'. 
	 */
	public Song(Context context, String songFileName) throws NumberFormatException, IOException 
	{
		name = songFileName; 
		
		FileInputStream fStream = context.openFileInput(songFileName);
		BufferedReader br = new BufferedReader(new InputStreamReader(fStream));
		tempoIndex = Integer.valueOf(br.readLine());
		soundResIDArray = new SoundResIDArray(br.readLine());
		int numColumns = Integer.valueOf(br.readLine());
		int numRows = soundResIDArray.getSize();
		
		Log.v("MsgFilter", "here");
		
		mAdapter = new GridAdapter(context, numRows, numColumns);
		for (int n = 0; n < numColumns * numRows; n++)
		{
			String line = br.readLine();
			if (line == null) continue;
			
			int stateIndex = Integer.valueOf(line);
			mAdapter.setValue(n, State.values()[stateIndex]);
		}
		
		Log.v("MsgFilter", "here again");
		
		br.close();
		fStream.close();
		
		assert(tempoIndex >= 0 && tempoIndex < tempoArray.length);
		assert(numColumns >= MIN_COLUMNS);
		
		Log.v("MsgFilter", "there");
	}
	
	public Song(Parcel in) throws NumberFormatException, IOException
	{
		name = in.readString();
		soundResIDArray = new SoundResIDArray(in.readString());
		tempoIndex = in.readInt();
		
		int numColumns = in.readInt();
		int numRows = in.readInt();
		
		int len = numColumns * numRows;
		boolean[] array = new boolean[len];
		in.readBooleanArray(array);
		
		mAdapter = new GridAdapter(MainActivity.self(), numRows, numColumns);
		for (int n = 0; n < len; n++)
		{
			if (array[n]) 	mAdapter.setValue(n, State.ON);
			else			mAdapter.setValue(n, State.OFF);
		}
		
		assert(tempoIndex >= 0 && tempoIndex < tempoArray.length);
		assert(numColumns >= MIN_COLUMNS);
	}
	
	public Song(String soundArrayFileName, Context context) throws FileNotFoundException, IOException {
		soundResIDArray = new SoundResIDArray(soundArrayFileName);
		mAdapter = buildGridAdapter(context);
	}
	
	public Song(Context context) throws FileNotFoundException, IOException {
		soundResIDArray = new SoundResIDArray(0);
		mAdapter = buildGridAdapter(context);
	}
	
	public void increaseTempo()
	{
		if (tempoIndex < tempoArray.length - 1)
			tempoIndex++;
	}
	
	public void decreaseTempo()
	{
		if (tempoIndex > 0)
			tempoIndex--;
	}
	
	public String getName() { return name; }
	
	public int getNumRows() { return mAdapter.getNumRows(); }
	
	public int getNumColumns() { return mAdapter.getNumColumns(); }
	
	public State getCellState(int row, int column) { return mAdapter.getState(row, column); }
	
	public String getSoundArrayName() { return soundResIDArray.getName(); }
	
	public int getSoundResID(int row) { return soundResIDArray.getResID(row); }
	
	public String[] getSoundNameArray() 
	{
		String[] result = new String[soundResIDArray.getSize()];
		
		for (int i = 0; i < soundResIDArray.getSize(); ++i) 
		{
			result[i] = soundResIDArray.getName(i);
		}
		
		return result;
	}
	
	public int getNumActiveRows(int column) {
		int result = 0;
		for (int r = 0; r < getNumRows(); ++r) {
			if (mAdapter.getState(r, column).isOn()) {
				++result;
			}
		}
		return result;
	}
	
	public int getTempo() { return tempoArray[tempoIndex]; }
	
	/*
	 * - saves this Song AND its SoundArray, each in its own file. 
	 */
	public void save() throws IOException 
	{ 
		FileOutputStream fStream = null;
				
		try { fStream = MainActivity.self().openFileOutput(name + fileExtension, Context.MODE_PRIVATE); } 
		catch (FileNotFoundException ex) { Log.v("MsgFilter", "Could not create file " + name); }
		
		OutputStreamWriter osw = new OutputStreamWriter(fStream);
		osw.write(tempoIndex + "\n");
		osw.write(soundResIDArray.getName() + "\n");
		osw.write(getNumColumns() + "\n");
		soundResIDArray.save();
		
		for (int n = 0; n < getNumRows() * getNumColumns(); n++) 
		{
			int stateIndex = mAdapter.getState(n).ordinal();
			osw.write(stateIndex + "\n");
		}
		
		osw.close();
		fStream.close();
	}
	
	public void saveSoundResIDArray() throws IOException 
	{
		soundResIDArray.save();
	}
	
	public void setName(String newName) 
	{
		name = newName;
	}
	
	public void setSoundArrayName(String newName) 
	{
		soundResIDArray.setName(newName);
	}
	
	public void setSoundResIDArray(int type) throws FileNotFoundException, IOException
	{
		soundResIDArray = new SoundResIDArray(type);
		MixerPlayer.getInstance().updateWavArrays();
	}
	
	public void setNumColumns(int num) 
	{
		assert(num >= MIN_COLUMNS);
		
		mAdapter.setNumColumns(num);
		MixerPlayer.getInstance().setNumTracks(num);
	}
	
	public void loadSoundArray(String fileName) throws NumberFormatException, IOException 
	{
		soundResIDArray = new SoundResIDArray(fileName);
		MixerPlayer.getInstance().updateWavArrays();
	}
	
	public void setAdapterToGrid(GridView v) 
	{
		v.setAdapter(mAdapter);
	}
	
	public void clearGrid()
	{
		mAdapter.clearAll();
		MixerPlayer.getInstance().mixAll();
	}
	
	private GridAdapter buildGridAdapter(Context context)
	{
		return new GridAdapter(context, soundResIDArray.getSize());
	}

	@Override
	public int describeContents() { return 0; }

	@Override
	public void writeToParcel(Parcel out, int flags) 
	{
		int columns = mAdapter.getNumColumns();
		int rows = mAdapter.getNumRows();
		
		out.writeString(name);
		out.writeString(soundResIDArray.getName());
		out.writeInt(tempoIndex);
        out.writeInt(columns);
        out.writeInt(rows);
        
        int len = columns * rows;
        boolean[] array = new boolean[len];
        for (int i = 0; i < len; i++)
        {
            State value = mAdapter.getState(i);
        	
            if (value == State.ON || value == State.SELECTED)
            		array[i] = true;
        }
        
        out.writeBooleanArray(array);
	}
	
	public static final Parcelable.Creator<Song> CREATOR = new Parcelable.Creator<Song>() 
	{
		@Override
		public Song createFromParcel(Parcel source) 
		{
			try { return new Song(source); }
			catch (NumberFormatException e) { e.printStackTrace(); } 
			catch (IOException e) { e.printStackTrace(); }
			catch (Exception e) { Log.v("MsgFilter", "UNKNOWN ERROR OCCURED");}
			
			Log.v("MsgFilter", "NO SONG CREATED");
			return null;
		}

		@Override
		public Song[] newArray(int size)
		{
			return new Song[size];
		}
	};
}
