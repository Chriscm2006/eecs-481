package edu.umich.jammaster2;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import ask.scanninglibrary.ASKActivity;

public class FileDialogActivity extends ASKActivity 
{
	private Song song;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_dialog);
		
		song = (Song) getIntent().getParcelableExtra("song");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.file_dialog, menu);
		return true;
	}

	public void Load (View view)
	{
		Intent loadFile = new Intent(this, LoadFileActivity.class);
		startActivityForResult(loadFile, 0);
	}
	
	public void Save (View view)
	{
		Intent saveFile = new Intent(this, SaveFileActivity.class);
		saveFile.putExtra("song", song);
		startActivityForResult(saveFile, 1);
	}
	
	public void Delete (View view)
	{
		Intent deleteFile = new Intent(this, DeleteFileActivity.class);
		startActivity(deleteFile);
	}
	
	public void Back (View view)
	{
		finish();
	}
	
	@Override 
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{     
		super.onActivityResult(requestCode, resultCode, data);
		
		if (resultCode == Activity.RESULT_CANCELED) return;
		
		setResult(Activity.RESULT_OK, data);
		finish();
	}
}
