package edu.umich.jammaster2;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

/*
 * - A sound resource given to this class must be a stereo .wav file with a 16-bit sample 
 * 		size, and a 44100hz sample rate. 
 * - There is a maximum length wav file that can be supplied. That length is probably about 10 seconds. 
 * - For now, this class assumes that only one Song exists. 
 * - In this class, the definitions of 'position', 'frame', etc. in the context of sound files 
 * 		match those used in the AudioTrack API. 
 * - You'll see "arrInd" or just "ind" in some names. That refers to the array index of a 
 * 		wav array. That index is twice the corresponding 'position' of an AudioTrack. 
 */
public class MixerPlayer 
{
	public static final int SAMPLE_RATE = 44100; // hz
	public static final int IND_PER_SEC = SAMPLE_RATE * 2; // accounts for stereo. 
	private static final int TRACK_LENGTH_IND = IND_PER_SEC * 2; // 2 seconds worth
	
	private ArrayList<AudioTrack> tracks = new ArrayList<AudioTrack>();
	
	// The arrays in 'wavArrays' should not be longer than necessary to hold the sound data. 
	private ArrayList<short[]> wavArrays = new ArrayList<short[]>(); // indexed by row, then sound .wav array position. 
	private boolean isPlaying = false;
	
	private final static MixerPlayer INSTANCE = new MixerPlayer();
	private ReadOnlySong song;
	
	
	private MixerPlayer() {}
	
	public static MixerPlayer getInstance()
	{
		return INSTANCE;
	}
	
	// Until you give it a song, the MixerPlayer is not initialized
	public void setSong(ReadOnlySong song)
	{
		this.song = song;
		setNumTracks(song.getNumColumns());
		updateWavArrays();
	}
	
	public void setWavArrays() 
	{
		ArrayList<short[]> tempWavs = new ArrayList<short[]>();
		
		for (int i = 0; i < song.getNumRows(); ++i) 
		{
			tempWavs.add(buildWavArray(song.getSoundResID(i)));
		}
		
		wavArrays = tempWavs;
	}
	
	public void setNumTracks(int numTracks) 
	{
		ArrayList<AudioTrack> tempTracks = new ArrayList<AudioTrack>();
		
		short[] mix = new short[TRACK_LENGTH_IND];
		
		for (int i = 0; i < numTracks; ++i) 
		{
			AudioTrack a = 
			new AudioTrack(AudioManager.STREAM_MUSIC, SAMPLE_RATE, 
						   AudioFormat.CHANNEL_OUT_STEREO, 
						   AudioFormat.ENCODING_PCM_16BIT, 
						   TRACK_LENGTH_IND, 
						   AudioTrack.MODE_STATIC);
			
			a.write(mix, 0, TRACK_LENGTH_IND);
			tempTracks.add(a);
		}
		
		tracks = tempTracks;
	}
	
	// start async playback 
	public void play() 
	{
		assert(song != null);
		isPlaying = true;
		
		new Thread(new Runnable() 
		{
			public void run() 
			{
				int track = 0;
				
				while (isPlaying) 
				{
					if (track >= song.getNumColumns())
						track = 0;
					
					AudioTrack a = tracks.get(track);
					long tempo = (long) (60000.0 / song.getTempo());
					
					reset(a);
					a.play();
					
					try { Thread.sleep(tempo); } 
					catch (InterruptedException e) 
					{ e.printStackTrace(); }
					
					track++;
				}
			}
		}).start();
	}
	
	// stop async playback
	public void stop() 
	{
		isPlaying = false;
	}
	
	public void updateWavArrays()
	{
		setWavArrays();
		mixAll();
	}
	
	/*
	 * - This should be called when just a cell in a Song changes. 
	 */
	public void updateSongCell(GridIndex coords) 
	{
		mixColumn(coords.column);
	}
	
	public void mixAll() 
	{
		for (int i = 0; i < tracks.size(); ++i) 
		{
			mixColumn(i);
		}
	}
	
	/*
	 * - combines the Sounds' arrays into a single array, and writes the array to 'audioTrack'. 
	 * - temporarily pauses playback, resuming from the column at which playback stopped if 
	 * 		that column is not past the end of the new mix, and otherwise starting from 
	 * 		the beginning. 
	 * - 'endPos' - 1 is the last position mixed, unless it is past the end of the grid. 
	 */
	private void mixColumn(final int column) 
	{
		new Thread(new Runnable()
		{
			public void run()
			{
				assert(song != null);
				
				AudioTrack track = tracks.get(column);
				int numActiveRows = song.getNumActiveRows(column);
				short[] mix = new short[TRACK_LENGTH_IND];
				
				if (numActiveRows == 0) {
					writeSilence(track);
					return;
				}
				
				for (int mixInd = 0; mixInd < TRACK_LENGTH_IND; ++mixInd) 
				{
					int finalSample = 0;
					for (int r = 0; r < song.getNumRows(); ++r) 
					{
						if (song.getCellState(r, column).isOn())
						{
							if (wavArrays.get(r).length > mixInd)
							{
								finalSample += wavArrays.get(r)[mixInd];
							}
						}
					}
					
					mix[mixInd] = (short) (finalSample / numActiveRows);
				}
				
				track.write(mix, 0, TRACK_LENGTH_IND);
			}
		}).start();
	}
	
	private void writeSilence(AudioTrack track) {
		// we can't write a tiny array because it won't 
		// overwrite the rest of what was there. 
		track.write(new short[TRACK_LENGTH_IND], 0, TRACK_LENGTH_IND);
	}
	
	private static short[] buildWavArray(int wavFileResID) 
	{
		InputStream is = MainActivity.self().getResources().openRawResource(wavFileResID);
		DataInputStream dis = new DataInputStream(is);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		try 
		{
			for (int n; (n = dis.read()) != -1; ) 
			{
				baos.write(n);
			}
		} 
		
		catch (IOException e) 
		{ e.printStackTrace(); }
		
		ByteBuffer bb = ByteBuffer.wrap(baos.toByteArray());
		bb.order(ByteOrder.LITTLE_ENDIAN);
		ShortBuffer sb = bb.asShortBuffer();
		short[] result = new short[sb.capacity()];
		
		for (int i = 0; i < sb.capacity(); i++) 
		{
			result[i] = sb.get(i);
		}
		
		return result;
	}
	
	private void reset(AudioTrack a) 
	{
		if (a.getPlayState() == AudioTrack.PLAYSTATE_PLAYING) 
		{
			a.stop();
			a.reloadStaticData();
		}
	}
}
