package com.example.jammaster;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity {

	final ToneLocker toneLocker = ToneLocker.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	
	public void playA(View view)
	{
		toneLocker.play(0);
	}
	
	public void playB(View view)
	{
		toneLocker.play(1);
	}
	
	public void playC(View view)
	{
		toneLocker.play(2);
	}
	
	public void playD(View view)
	{
		toneLocker.play(3);
	}
	
	public void playE(View view)
	{
		toneLocker.play(4);
	}
	
	public void playF(View view)
	{
		toneLocker.play(5);
	}
	
	public void playG(View view)
	{
		toneLocker.play(6);
	}
	
	
	public void Next(View view)
	{
		Intent intent = new Intent(this, Composer.class);
        startActivity(intent);      
        finish();
	}
}
