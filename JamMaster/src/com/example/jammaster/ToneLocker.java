package com.example.jammaster;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

public class ToneLocker
{
	private static ToneLocker instance = null;
	
	double duration = 0.5;
	int sampleRate = 8000;
	int numSamples = (int)(duration * sampleRate);
	
	AudioTrack[] note = new AudioTrack[8];
	
	protected ToneLocker() 
	{
		double[] freq = {440.00, 493.88, 523.25, 587.33, 659.25, 698.46, 783.99, 880.00};
		
		for (int x = 0; x < note.length; x++)
		{
			note[x] = new AudioTrack(AudioManager.STREAM_MUSIC,
					8000, AudioFormat.CHANNEL_OUT_MONO,
					AudioFormat.ENCODING_PCM_16BIT, numSamples,
					AudioTrack.MODE_STATIC);
			
			note[x].write(genTone(freq[x]), 0, numSamples);
		}
	}
	
	public byte[] genTone(double freqOfTone)
	{
		double sample[] = new double[numSamples];
		byte generatedSnd[] = new byte[2 * numSamples];
		
		for (int i = 0; i < numSamples; ++i) 
			sample[i] = Math.sin((2 * Math.PI * i) / (sampleRate / freqOfTone)); 

		// Convert to 16 bit pcm sound array
		//  - Assumes the sample buffer is normalized.
		int idx = 0;
		for (double dVal : sample) 
		{
			short val = (short) (dVal * 32767);
			generatedSnd[idx++] = (byte) (val & 0x00ff);
			generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
		}
		
		return generatedSnd;
	}
	   
	public static ToneLocker getInstance() 
	{
	      if(instance == null)
	         instance = new ToneLocker();
	      
	      return instance;
	}
	
	public void play(int n)
	{
		if (note[n].getPlayState() == AudioTrack.PLAYSTATE_PLAYING)
		{
			note[n].stop();
			note[n].reloadStaticData();
		}
		
		note[n].play();
	}
	
	public void setRate(int n)
	{
		for (int x = 0; x < note.length; x++)
			note[x].setPlaybackRate(n);
	}
}
