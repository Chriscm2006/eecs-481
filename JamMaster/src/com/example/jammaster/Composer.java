package com.example.jammaster;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class Composer extends Activity {
	
private final ToneLocker toneLocker = ToneLocker.getInstance();
	
	//RowAdapter class allows Adapter with 
	//  array data source populated by LinearLayouts
	public class RowAdapter extends ArrayAdapter<LinearLayout>
	{
		public RowAdapter(Context context, int resource, List<LinearLayout> list) 
		{
			super(context, resource, list);
		}
			
		public LinearLayout getView(int a, View b, ViewGroup c)
		{
			return getItem(a);
		}
	}

	private int width = 8, height = 10;
	private boolean playing = false;
	TextView arrow;
	
	private List<LinearLayout> myRowArray;
	private RowAdapter myAdapter;
	private ListView myList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_composer);
		
		// Initialize ListView for horizontal addition of LinearLayouts
		myList = (ListView) findViewById(R.id.listView);
		myRowArray = new ArrayList<LinearLayout>();
		myAdapter = new RowAdapter(this, android.R.layout.simple_list_item_1, myRowArray);
		myList.setAdapter(myAdapter);
		
		// Populate the ListView with LinearLayouts
        for (int y = 0; y < height; y++)
        {
        	// Initialize and add new LinearLayout
        	LinearLayout h = new LinearLayout(this);
        	myRowArray.add(h);
        	
        	// Initialize and add ToneMarks to the LinearLayout
        	for (int x = 0; x < width; x++)
        		h.addView(new CheckBox(this));
        }
        
        // Update the Adapter
        myAdapter.notifyDataSetChanged();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.composer, menu);
		return true;
	}
	
	public void Back(View view)
	{
		Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);      
        finish();
	}
	
	public void Play(View view)
	{
		playing = true;
		
		Thread thread = new Thread(new Runnable() 
		{
			public void run()
			{
				int y = 0;
				
				while (playing) 
				{
					if (y >= myAdapter.getCount()) y = 0;
					
					LinearLayout h = myAdapter.getItem(y);
						  
					for (int x = 1; x < h.getChildCount(); x++)
					{
						CheckBox mark = (CheckBox) h.getChildAt(x);
						
						if (mark.isChecked()) 
							toneLocker.play(x);
					}
					
					try { Thread.sleep(500); } 
					catch (InterruptedException e) { e.printStackTrace(); }
					
					y++;
				}
			}
		});
		
		thread.start();
	}
	
	public void Stop(View view)
	{
		playing = false;
	}
	
	public void AddRow(View view)
	{
		// Initialize and add new LinearLayout
    	LinearLayout h = new LinearLayout(this);
    	myRowArray.add(h);

    	// Initialize and add ToneMarks to the LinearLayout
    	for (int x = 0; x < width; x++)
    		h.addView(new CheckBox(this));
    	
    	// Update the Adapter
    	myAdapter.notifyDataSetChanged();
	}
	
	public void RemoveRow(View view)
	{
		// Remove a LinearLayout from the 
		//  bottom of the adapter source array
		
		int count = myAdapter.getCount();
		
		if (count == 0)
			return;
		
		LinearLayout last = myAdapter.getItem(count - 1);
		myAdapter.remove(last);
		
		// Update the Adapter
    	myAdapter.notifyDataSetChanged();
	}
}
