package com.example.practice;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import ask.scanninglibrary.ASKActivity;

public class MainActivity extends ASKActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    public void pushMe(View view) {
        // Method for the pushMe button
    	System.out.println("Push Me!");
    	Intent i = new Intent(getApplicationContext(), SecondActivity.class);
    	startActivity(i);
    }
    
    public void noPushMe(View view) {
    	// Method for the noPushMe button
    	System.out.println("No, Push ME!");
    }
}
